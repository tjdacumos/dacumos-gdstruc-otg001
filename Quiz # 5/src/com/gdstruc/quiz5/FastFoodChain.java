package com.gdstruc.quiz5;

public class FastFoodChain {
    private String name;
    private int minCost;
    private int maxCost;

    public FastFoodChain(String name, int minCost, int maxCost) {
        this.name = name;
        this.minCost = minCost;
        this.maxCost = maxCost;
    }

    public int getMinCost() {
        return minCost;
    }

    public int getMaxCost() {
        return maxCost;
    }

    @Override
    public String toString() {
        return "" + name + ", " +
                "Price Range: Php" + minCost +
                " - Php" + maxCost;
    }
}
