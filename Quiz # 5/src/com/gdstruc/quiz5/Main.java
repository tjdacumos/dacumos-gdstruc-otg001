package com.gdstruc.quiz5;

public class Main {

    public static void main(String[] args) {

        saanTayo FastFoodChains = new saanTayo();
        FastFoodChains.insert("Mcdonalds", 50, 250);
        FastFoodChains.insert("Jollibee",50,200 );
        FastFoodChains.insert("Pepper Lunch", 170,430);
        FastFoodChains.insert("Siomai House", 15, 40);
        FastFoodChains.insert("Yellow Cab", 300, 1000);
        FastFoodChains.insert("Burger King", 100, 250);

        FastFoodChains.printListOfFoodChains();
        System.out.println("\nList of Food Chain(s) within your budget: ");
        FastFoodChains.searchWithinBudget(100,250);
        System.out.println("\nRandom Fast-Food Chain within your budget: ");
        System.out.println(FastFoodChains.randomWithinBudget(50,200));

    }
}
