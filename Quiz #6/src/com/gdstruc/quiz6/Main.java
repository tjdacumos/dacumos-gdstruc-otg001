package com.gdstruc.quiz6;

public class Main {

    public static void main(String[] args) {
	Tree tree =new Tree();

	tree.insert(25);
	tree.insert(17);
	tree.insert(29);
	tree.insert(10);
	tree.insert(16);
	tree.insert(-5);
	tree.insert(60);
	tree.insert(55);

	System.out.println("Traversing in ascending order");
	tree.traverseInOrder();
	System.out.println("Traversing in descending order: ");
	tree.traverseInOrderDescending();

	System.out.println("The minimum value is " + tree.getMin());
	System.out.println("The maximum value is " + tree.getMax());
    }
}
