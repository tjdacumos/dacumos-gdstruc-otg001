package com.gdstruc.midterm;

import java.util.Random;
import java.util.Scanner;
import java.util.logging.Handler;

public class Main {

    public static void main(String[] args) {
	// write your code here
        CardStack deck = new CardStack("Deck");
        CardStack hand = new CardStack("Hand");
        CardStack discardPile = new CardStack("Discard Pile");

        generateDeck(deck);
        int turns = 0;
        do {
            System.out.print("Randomed Command: ");
            switch ((int) (Math.random() * (3) + 1)) {
                //deck to hand
                case 1:
                    System.out.println("Player Draw Card");
                    transferCard(deck, hand);
                    break;
                //hand to discard pile
                case 2:
                    System.out.println("Player Discard Card");
                    transferCard(hand, discardPile);
                    break;
                //discard pile to hand
                case 3:
                    System.out.println("Player Retrieve Card from Discard Pile");
                    transferCard(discardPile, hand);
                    break;
            }
            printCardsState(hand, deck, discardPile);
            System.out.println("Press \"ENTER\" to continue...");
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
            turns++;
        }while(deck.isEmpty() != true);

        System.out.println("Program finished after " + turns + " turns");
    }

    public static void generateDeck(CardStack deck)
    {
        //generate card names
        String[] names = new String[5];

        names[0] = "Sir Dale";
        names[1] = "Sir Kevin";
        names[2] = "Sir Deng";
        names[3] = "Sir Norms";
        names[4] = "Sir Chris";

        for(int i = 0; i<30; i++) {

            Card cards = new Card(names[(int)(Math.random() * (5))], (int)(Math.random() * (10) + 1) * 1000);
            deck.addToStack(cards);
        }
    }
    //create commands per turn
    //exclude in cardstack class since they are commands
    public static void transferCard(CardStack from, CardStack to)
    {
        // random 5-1 times
        for(int i = 0; i <(int)(Math.random() * (5)+1); i++) {
            if(from.isEmpty() == true)
            {
                System.out.println("Stack status: " + from.getStackType() + " is empty");
                return;
            }
            to.addToStack(from.drawFromStack());
        }
    }

    public static void printCardsState(CardStack hand, CardStack deck, CardStack discardPile)
    {
        System.out.println("Player's hand: ");
        hand.printStack();
        System.out.println("Remaining cards in the deck: " + deck.getCount() +
                "\nNumber of cards in the discarded pile: " + discardPile.getCount());

    }

}

