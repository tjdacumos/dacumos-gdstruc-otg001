package com.gdstruc.midterm;

import java.util.LinkedList;
import java.util.ListIterator;

public class CardStack {
    private String stackType;
    private LinkedList<Card> stack;
    private int count;

    public CardStack(String type)
    {
        this.stackType = type;
        stack = new LinkedList<Card>();
    }

    public void addToStack(Card card)
    {
        count++;
        stack.push(card);
    }

    public Card drawFromStack()
    {
        count--;
        return stack.pop();

    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public int getCount() {
        return count;
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }
    public String getStackType() {
        return stackType;
    }

}
