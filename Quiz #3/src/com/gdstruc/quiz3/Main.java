package com.gdstruc.quiz3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayQueue matchmaking = new ArrayQueue(7);
        int id = 0;
        int gameCount = 0;
        int turns = 1;

        do{
        //queueing phase
        id += generatePlayers(matchmaking, id);
        System.out.println("\nPlayers in queue: ");
        matchmaking.printQueue();

        //game started phase
        if(matchmaking.size() >= 5) {
            gameStart(matchmaking);
            gameCount++;
        }
        else
        {
            System.out.println("Not enough players yet!");
        }
        pressEnter();

        turns++;
        }while(gameCount < 10);

        //not really needed to print out
        if(matchmaking.size()>1)
        {
            System.out.println("Players still in queue: ");
            matchmaking.printQueue();
        }

        System.out.println("Turns to complete "+ gameCount + " games: " + turns);
    }

    //matchmaking mechanics
    //assume players won't requeue
    //generate random player information +
    //every turn x players queue
    //int for the purpose of having an incremental id
    public static int generatePlayers(ArrayQueue matchmaking, int id)
    {
        int inQueue = (int)(Math.random()*6 + 1);

        String[] names = new String[6];
        String[] titles = new String[6];

        System.out.println("Number of new queueing players: " + inQueue);

        names[0] = "Sir Dale";
        names[1] = "Sir Kevin";
        names[2] = "Sir Deng";
        names[3] = "Sir Norms";
        names[4] = "Sir Chris";
        names[5] = "Ms. Loraine";
        titles[0] = " The Favorite";
        titles[1] = " The Imposter";
        titles[2] = " The Terror";
        titles[3] = " The Solo Player";
        titles[4] = " The Laid Back";
        titles[5] = " The Energetic";

        for(int i = 0; i < inQueue; i++) {
            Player players = new Player(id,
                    names[(int) (Math.random() * (6))] + titles[(int) (Math.random() * (6))],
                    (int) (Math.random() * (20) + 80));
            id++;
            matchmaking.add(players);
        }
        return inQueue;
    }

    //game starts when at least 5 players are in queue +
    //pop the first 5 players
    public static void gameStart(ArrayQueue matchmaking)
    {
        System.out.println("\nA game started... removing 5 players in queue: ");
        for(int i = 0; i < 5; i++)
        {
            System.out.println(matchmaking.remove());
        }
    }
    public static void pressEnter()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press \"ENTER\" to continue...");
        scanner.nextLine();
    }

}
