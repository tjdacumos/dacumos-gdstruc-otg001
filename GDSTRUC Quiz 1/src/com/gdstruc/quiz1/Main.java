package com.gdstruc.quiz1;

import java.util.Random;

public class Main {

    public static void main(String[] args)
    {
	int[] numbers = new int[10];

    randomArray(numbers, -100, 100);
    System.out.println("Before bubble sort: ");
    printArray(numbers);

    System.out.println("\nAfter bubble sort: ");
    bubbleSort(numbers);
    printArray(numbers);

    randomArray(numbers, -100, 100);
    System.out.println("\n\nBefore selection sort: ");
    printArray(numbers);

    System.out.println("\nAfter selection sort: ");
    selectionSort(numbers);
    printArray(numbers);
    }
    private static void randomArray(int[] arr, int min, int max)
    {
        Random random = new Random();
        for (int i = 0; i < arr.length; i++)
        {
            int rand = random.nextInt(max - min + 1) + min;
            arr[i] = rand;
        }
    }
    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length -1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex]) //finding smallest integer
                {
                    smallestIndex = i;
                }
            }

            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;

        }
    }
    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i <lastSortedIndex; i++)
            {
                if(arr[i] < arr [i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }
    private static void printArray(int[] arr)
    {
        for (int j: arr )
        {
            System.out.print(j + " ");
        }
    }
}

