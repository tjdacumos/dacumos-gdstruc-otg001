package com.gdstruc.quiz2;

public class Main {

    public static void main(String[] args) {
        Player asuna = new Player(1,"Asuna",100);
        Player lethalBacon = new Player(2,"LethalBacon",205);
        Player hpDeskjet = new Player(3,"HPDeskJet",34);
        Player kirito = new Player(100,"Kirito", 1000);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);

        System.out.println("Before removing front");
        playerLinkedList.printList();
        System.out.println("List size: " + playerLinkedList.getSize());
        System.out.println("\nDoes it contain player kirito? " + playerLinkedList.contains(kirito));
        System.out.println("What index?" + playerLinkedList.indexOf(kirito));
        System.out.println("Does it contain player lethalBacon? " + playerLinkedList.contains(lethalBacon));
        System.out.println("What index?" + playerLinkedList.indexOf(lethalBacon));
        System.out.println("Does it contain player hpDeskjet? " + playerLinkedList.contains(hpDeskjet));
        System.out.println("What index?" + playerLinkedList.indexOf(hpDeskjet));

        System.out.println("\nAfter removing front");
        playerLinkedList.removeFront();
        playerLinkedList.printList();
        System.out.println("List size: " + playerLinkedList.getSize());

        System.out.println("\nDoes it contain player kirito? " + playerLinkedList.contains(kirito));
        System.out.println("What index?" + playerLinkedList.indexOf(kirito));
        System.out.println("Does it contain player lethalBacon? " + playerLinkedList.contains(lethalBacon));
        System.out.println("What index?" + playerLinkedList.indexOf(lethalBacon));
        System.out.println("Does it contain player hpDeskjet? " + playerLinkedList.contains(hpDeskjet));
        System.out.println("What index?" + playerLinkedList.indexOf(hpDeskjet));

    }
}
