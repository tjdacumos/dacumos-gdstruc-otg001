package com.gdstruc.quiz2;

public class PlayerLinkedList {
    private PlayerNode head;
    private int size = 0;


    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
        size++;
    }

    public void removeFront()
    {
        PlayerNode front = head;
        front = front.getNextPlayer();
        head = front;
        size--;
    }

    public int getSize() {
        return size;
    }

    public boolean contains(Player player)
    {
        PlayerNode current = head;
       for (int i = 0; i < size; i++)
       {
           if (player != null && player.equals(current.getPlayer()))
           {
            return true;
           }
           current = current.getNextPlayer();
       }
        return false;
    }

   public int indexOf(Player player) {
       PlayerNode current = head;

       for (int index = 0; index < size; index++) {
           if (player != null && player.equals(current.getPlayer())) {
               return index;
           }
           current = current.getNextPlayer();
       }
       return -1;
   }

    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while(current != null){
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }
}
